<?php
/**
 * Date: 08/08/2018
 * Time: 16:20
 * @author Artur Bartczak <artur.bartczak@code4.pl>
 */

namespace Proexe\BookingApp\Utilities;


class DistanceCalculator {
const earthR= 6371000;//promień ziemi
	/**
	 * @param array  $from
	 * @param array  $to
	 * @param string $unit - m, km
	 *
	 * @return mixed
	 */
	public function calculate( $from, $to, $unit = 'm' ) {
		//var_dump($from, $to, $unit);
		$lat_from= deg2rad($from['lat']);
		$lng_from= deg2rad($from['lng']);
		$lat_to= deg2rad($to['lat']);
		$lng_to= deg2rad($to['lng']);		
		

		//$latDelta = $lat_to - $lat_from;
		$lonDelta = $lng_to - $lng_from;

		$a = pow(cos($lat_to) * sin($lonDelta), 2) +
		pow(cos($lat_from) * sin($lat_to) - sin($lat_from) * cos($lat_to) * cos($lonDelta), 2);
	  	$b = sin($lat_from) * sin($lat_to) + cos($lat_from) * cos($lat_to) * cos($lonDelta);
	
	  	$angle = atan2(sqrt($a), $b);
		
		$distance = $angle * DistanceCalculator::earthR;
		//return $distance;
		if($unit == 'km'){
			return $distance/1000;
		}elseif($unit == 'm'){
			return $distance;
		}else{
			echo 'Wrong type of unit';
		}
		
	}

	/**
	 * @param array $from
	 * @param array $offices
	 *
	 * @return array
	 */
	public function findClosestOffice( $from, $offices ) {
		$distance=0;
		$earthCir=40075; //obwód ziemi
		foreach ($offices as $office){
			$distance = $this->calculate($from, ['lat'=> $office['lat'], 'lng'=>$office['lng']], 'km');
			if($distance<$earthCir){
				$destination=$office;
			}
		}
		return $destination;
	}
//TASK3 Usign Mysql for finding nearest office
// $query ="SELECT *,((ACOS(SIN(PI()*".$lat_from."/180.0)*SIN(PI()*".$lat_to."/180.0)+COS(PI()*".$lat_from."/180.0)*COS(PI()*".$lat_to."/180.0)*COS(PI()*".$lng_to."/180.0-PI()*".$lng_from."/180.0))*6371)*1000) as distance
// FROM `offices`
// WHERE distance >= ".$distance."";
	
}