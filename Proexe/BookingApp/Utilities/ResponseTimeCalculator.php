<?php
/**
 * Date: 09/08/2018
 * Time: 00:16
 * @author Artur Bartczak <artur.bartczak@code4.pl>
 */

namespace Proexe\BookingApp\Utilities;
use Proexe\BookingApp\Offices\Interfaces\ResponseTimeCalculatorInterface;

class ResponseTimeCalculator implements ResponseTimeCalculatorInterface {


    private function calculate_one_day (\DateTime $Start, \DateTime $Stop, int $number_of_days, int $day, array $officeHours) {

        $work_hours_in_one_day = $this->Work_Time( $officeHours, $Start->modify("+$day day") );
        if ( $work_hours_in_one_day['isClosed']  === false ) {


            if ( $number_of_days === 1 ) {
                $time_start = max( strtotime($Start->format('H:i')), strtotime($work_hours_in_one_day['from']) );
                $time_stop = min( strtotime($Stop->format('H:i')), strtotime($work_hours_in_one_day['to']) );
            }
            else if ( $day === 0 ) {
                $time_start = max( strtotime($Start->format('H:i')), strtotime($work_hours_in_one_day['from']) );
                $time_stop = strtotime($work_hours_in_one_day['to']);
            }
            else {
                $time_start = strtotime($work_hours_in_one_day['from']);
                $time_stop = strtotime($work_hours_in_one_day['to']);
            }


            $time_space_seconds = (($time_stop - $time_start) > 0) ? ($time_stop - $time_start) : 0 ;
        }

        return $time_space_seconds ?? 0 ;
    }


    private function Work_Time ( array $officeHours, \DateTime $date ) {
        $time_stamp = $date->getTimestamp();
        $day_week = date('w', $time_stamp);
        return $hours=$officeHours[$day_week];
	}
	public function calculate ( $bookingDateTime, $responseDateTime, $officeHours ) {

        $Start = new \DateTime( $bookingDateTime );
        $Stop = new \DateTime( $responseDateTime );

        $booking_date = (int) $Start->format('z');
        $response_date = (int)  $Stop->format('z');


        $number_of_days = 1+ $response_date - $booking_date ;



        $response_time = 0;
        for ($i = 0; $i < $number_of_days; $i++) {

            $response_time_for_one_day = $this->calculate_one_day( $Start, $Stop, $number_of_days, $i, $officeHours);
            $response_time += $response_time_for_one_day ?? 0;
        }
		$response_time_real=$response_time/60;
        return $response_time_real;
    }
}